# Syllabus (`adnsyllabus.cls`)

This class allows the creation of custom syllabus.

```latex
% Compile: pdflatex - biber - pdflatex
\documentclass{adnsyllabus}

\syllabusset{format=spanish.syllabus}

% bibliography embed in the same file
\usepackage{filecontents}% create files
\begin{filecontents}{program.bib}
@BOOK{Foo,
  author = {F. Foo},
  title = {Foo Book},
  publisher = {Foo Publisher},
  year = {2013},
  keywords={mandatory}% define type of reference
}

@BOOK{Bar,
  author = {Bar, B.},
  title = {Bar Book},
  publisher = {Bar Publisher},
  year = {2013},
  edition = {2},
  keywords={complementary}
}
\end{filecontents}

% Include bibliography file
\usepackage[backend=biber,style=ieee]{biblatex}
\addbibresource{program.bib}

%% Parameters of the course
% Course name
\classname{On the creation of nice syllabus}
% Standard values that can be redefined here
% \subtitle{\large COURSE PROGRAM}
% \author{%
%   Institute of Computing
% }
% \date{}

\begin{document}
% Header
\maketitle

\section{Identification}
% Code
\code{IT-XXXX}
% Credits
\credits{X}
% Duration or time
\duration{Semester}
% Semester in which is taught
\semester{First}
% Pre requisites 
\requirements{IT-001, IT-002}
% Sessions
\sessions{2 lectures, 1 laboratory}
% Make course identification
\makecourseid

\section{Learning Objectives}

% Define the objectives

\section{Contents}

% Contents
% We can use the following list format
\begin{enumerate}
\content{Topic 1}{Description.}
\content{Topic 2}{Description.}
\end{enumerate}

\section{Relevance}

% Description of the relevance of the course

\section{Methodology}

% Description of methods to follow.

\section{Evaluation}

% Description of the evaluation mechanisms

% Grades and activities
\begin{tabular}{lll}
\textbullet\quad Tests & 30\% & (15\% each) \\
\textbullet\quad Exam & 30\% &\\
\textbullet\quad Homeworks & 10\% &\\
\textbullet\quad Projects & 30\% &(15\% each) \\
\end{tabular}

% Include references
\section{References}
\nocite{*}% Include all references
% Mandatory references
\printbibliography[keyword=mandatory, title={Mandatory references},heading=subbibliography]
% Complementary
\printbibliography[keyword=complementary, title={Complementary references},heading=subbibliography]
\end{document}
```

